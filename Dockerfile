FROM registry.gitlab.com/sebastians90/docker-latex:latest

RUN apt-get update -qq -y \
 && apt-get install lhs2tex make -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
