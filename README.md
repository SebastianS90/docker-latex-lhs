# Docker Image for LaTeX with lhs2TeX

**Image Location**: `registry.gitlab.com/sebastians90/docker-latex-lhs:latest` (built by [GiltLab CI](https://gitlab.com/SebastianS90/docker-latex-lhs/pipelines))

This Docker image is based on my [docker-latex](https://gitlab.com/SebastianS90/docker-latex) image which is based on [Debian](https://hub.docker.com/_/debian/).
The following packages are installed:

- [`texlive-full`](https://packages.debian.org/stable/texlive-full) (metapackage pulling in all components of TeX Live)
- [`lhs2tex`](https://packages.debian.org/stable/lhs2tex) (generates LaTeX code from literate Haskell sources)
- [`make`](https://packages.debian.org/stable/make) (utility for directing compilation)

## License
This Dockerfile and the CI configuration is released into the public domain. Please see the [LICENSE](LICENSE) file for details.
For the license of software that is downloaded by this Dockerfile, please refer to their appropriate vendors.
